package kubernetes

import (
	"log"
	"time"

	http "gitlab.com/hbouvier/http"
)

type Kubernetes struct {
	client *http.Client
}

type ServerAddressByClientCIDR struct {
	ClientCIDR    string `json:"clientCIDR"`
	ServerAddress string `json:"serverAddress"`
}

type KubernetesAPIVersions struct {
	Kind                      string                      `json:"kind"`
	Versions                  []string                    `json:"versions"`
	serverAddressByClientCIDR []ServerAddressByClientCIDR `json:"serverAddressByClientCIDRs"`
}

func Connect(url string) *Kubernetes {
	client := http.New(url, nil, map[string]string{"Content-Type": "application/json; charset=UTF8"})
	return &Kubernetes{client: &client}
}

func (server *Kubernetes) WaitForKubernetesToBeReady() {
	var apiVersions KubernetesAPIVersions
	for {
		_, err := server.client.Get("/api", nil, &apiVersions)
		if err != nil {
			log.Println(err)
			time.Sleep(time.Second)
			continue
		}
		return
	}
}
