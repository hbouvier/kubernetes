package main

import (
	"log"
	"os"

	"gitlab.com/hbouvier/kubernetes"
)

func main() {
	server := kubernetes.Connect("http://127.0.0.1:8001")
	server.WaitForKubernetesToBeReady()
	namespaces, err := server.GetNamespaces()
	if err != nil {
		log.Println(err)
		os.Exit(0)
	}
	for _, namespace := range namespaces.Items {
		log.Println(namespace.Metadata.Name)
	}
}
