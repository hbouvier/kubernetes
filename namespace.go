package kubernetes

import (
	"errors"
)

var ErrNotExist = errors.New("object does not exist")

type NamespaceList struct {
	Items []Namespace `json:"items"`
}

type Namespace struct {
	Metadata Metadata `json:"metadata"`
}

type Metadata struct {
	Name        string            `json:"name"`
	Namespace   string            `json:"namespace"`
	Annotations map[string]string `json:"annotations,omitempty"`
}

func (server *Kubernetes) GetNamespaces() (*NamespaceList, error) {
	var namespaces NamespaceList

	_, err := server.client.Get("/api/v1/namespaces", nil, &namespaces)
	if err != nil {
		return nil, err
	}
	return &namespaces, nil
}
